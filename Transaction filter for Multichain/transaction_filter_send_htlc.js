/*Author : Lallemand Allan 
Done for Master Thesis : Atomic asset transfer between Multichain and Hyperledger Fabric.

This transaction filter for Multichain proposes a format for the HTLC on the form : ["htlc",senderAddress*,receiverAddress,blockHeightUnlock,hashlockHash,hashlockHashFunction].
*senderAddress was included for better understanding but it is not necessary because Multichain includes (and autorises to get the value for use in the transaction filters) already this value in the transaction.
Until that the first parameter is misspelled or missing in the json, this transction filter will verify that the other parameters are present and have the correct type.
If the first parameter is present (and correctly spelled), this transaction filter will reject the transactions with missing parameters and bad typed parameters (according to this format).

A second transaction filter named "transaction_filter_receive_htlc.js" will enforce the conditions of the HTLC based on this format

Since that the transaction filter must be given to the Multichain JSON-RPC API, the code must be written on only one line.
Before use, it is necessary to remove all end-of-line (which is also the reason why there is no comment with "//") */

/*For all parameters in the HTLC, verify that the type is correct.
 If all parameters have the correct type, return an empty string. Otherwise, return the name of all bad typed parameters and precises the attended type or value*/
function checkTypeValue(json)
{
	var badTypeFields = "";
	const availableHashFunctions = ["sha256"]; /*Here, we will use only one hash function. However, we have preferred put this variable in an array since that it could useful to allow users to choose the hash function that they want (and so it could be updated by administrators in a real setting)*/
	const availableHashFunctionsString = "sha256";
	if (!(json.senderAddress.constructor===String))
	{
		badTypeFields+= "senderAddress is not a String,"; 
	}
	if (!(json.receiverAddress.constructor===String))
	{
		badTypeFields+= "receiverAddress is not a String,"; 
	}
	if (!(json.blockHeightUnlock.constructor===Number))
	{
		badTypeFields+= "blockHeightUnlock is not a String,"; 
	}
	if (!(json.hashlockHash.constructor===String))
	{
		badTypeFields+= "hashlockHash is not a String,"; 
	}
	if (!(json.hashlockHashFunction == "sha256"))
	{
		badTypeFields += "Unknown hashing function was used. Available hash functions are : " + availableHashFunctionsString + ",";
	}
	if (!(badTypeFields == ""))
	{
		badTypeFields = badTypeFields.slice(0,-1);
	}
	return badTypeFields;
}

/*Verify that all parameters for a HTLC are present.
 If all parameters are present, return an empty string. Otherwise, return the name of all missing parameters*/
function checkPresenceValues(json)
{
	var missingFields = "";
	if (!("senderAddress" in json))
	{
		missingFields+= "senderAddress,"; 
	}
	if (!("receiverAddress" in json))
	{
		missingFields+= "receiverAddress,"; 
	}
	if (!("blockHeightUnlock" in json))
	{
		missingFields+= "blockHeightUnlock,"; 
	}
	if (!("hashlockHash" in json))
	{
		missingFields+= "hashlockHash,"; 
	}
	if (!("hashlockHashFunction" in json))
	{
		missingFields+= "hashlockHashFunction,"
	}
	if (!(missingFields == ""))
	{
		missingFields = missingFields.slice(0,-1);
	}
	return missingFields;
}

/*Look in the json field if the transaction is a correct sent HTLC or not.
 If the transaction is not a HTLC, does nothingIf the transaction is a HTLC, verify that all parameters for this HTLC are correct and reject the transaction if not*/
function jsonManagement(json)
{
	if ("mode" in json && json.mode == "htlc")
	{
		/*In this case, the transaction that appears here is considerated as a HTLC (send) and must respect some conditions. Otherwise, the transaction will be rejected*/
		var missingFields = checkPresenceValues(json);
		if (missingFields == "")
		{
			var badTypeFields = checkTypeValue(json);
			if (badTypeFields == "")
			{
				/*Considerated as a HTLC with no problem detected*/
				return null;
			}
			else
			{
				return "These fields have not the correct type/value for the HTLC : " + badTypeFields;
			}
		}
		else
		{
			return "These fields are not present for the HTLC : " + missingFields;
		}
	}
	/*Else : Not considerated as a HTLC*/
}

/*Look in the data field if the transaction is a correct sent HTLC or not.
 If the transaction is not a HTLC, does nothing.
 If the transaction is a HTLC, verify that all parameters for this HTLC are correct and reject the transaction if not*/
function dataManagement(data)
{
	if ("json" in data)
	{
		return jsonManagement(data.json);
	}
	else
	{
		/*Not considerated as a HTLC*/	
		return null;
	}
}

/*Look in a output if the transaction is a correct sent HTLC, a malformed HTLC or not a HTLC.
 If the transaction is not a HTLC, does nothing.
 If the transaction is a HTLC, verify that all parameters for this HTLC are correct and if not, returns the reason of the error*/
function voutManagement(vout)
{	
	if ("data" in vout)
	{	
		var n = 0;
		var resultLastData = null;
		while (n < vout.data.length && resultLastData == null) /* Until all datas of this output were inspected or that a malformed HTLC was detected */
		{
			resultLastData = dataManagement(vout.data[n]);
			n = n + 1;
		}
		return resultLastData; /* Return null if all HTLCs in the datas of this ouptut were considerated as correctly formated*/
	}
	else
	{	/*Not considerated as a HTLC*/
		return null;
	}
}

/*Main function
 Look if the transaction if a sent HTLC or not.
 If the transaction is not a HTLC, does nothing.
 If this is a sent HTLC, verify that all parameters are correct and rejects the transaction of not*/
function filtertransaction()
{	
	var vouts=getfiltertransaction().vout; /* Contains all outputs of the transaction*/
	var n = 0;
	var resultLastData = null; /* Contains a string explaining the error (if no error, contains null)*/
	while (n < vouts.length && resultLastData == null) /* Until all outputs were inspected or that a malformed HTLC was detected */
	{	
		resultLastData = voutManagement(vouts[n]);
		n = n + 1;
	}	
	if (resultLastData != null)	
	{
		return resultLastData; /* If a malformed HTLC is detected, rejects the transactions by returning the explanation if the error */
	}
}
