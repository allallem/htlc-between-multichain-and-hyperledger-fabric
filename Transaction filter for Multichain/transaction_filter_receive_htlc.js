/* Code for sha256 comes from https://geraintluff.github.io/sha256/sha256.min.js */
var sha256=function a(b){function c(a,b){return a>>>b|a<<32-b}for(var d,e,f=Math.pow,g=f(2,32),h="length",i="",j=[],k=8*b[h],l=a.h=a.h||[],m=a.k=a.k||[],n=m[h],o={},p=2;64>n;p++)if(!o[p]){for(d=0;313>d;d+=p)o[d]=p;l[n]=f(p,.5)*g|0,m[n++]=f(p,1/3)*g|0}for(b+=String.fromCharCode(128);b[h]%64-56;)b+=String.fromCharCode(0);for(d=0;d<b[h];d++){if(e=b.charCodeAt(d),e>>8)return;j[d>>2]|=e<<(3-d)%4*8}for(j[j[h]]=k/g|0,j[j[h]]=k,e=0;e<j[h];){var q=j.slice(e,e+=16),r=l;for(l=l.slice(0,8),d=0;64>d;d++){var s=q[d-15],t=q[d-2],u=l[0],v=l[4],w=l[7]+(c(v,6)^c(v,11)^c(v,25))+(v&l[5]^~v&l[6])+m[d]+(q[d]=16>d?q[d]:q[d-16]+(c(s,7)^c(s,18)^s>>>3)+q[d-7]+(c(t,17)^c(t,19)^t>>>10)|0),x=(c(u,2)^c(u,13)^c(u,22))+(u&l[1]^u&l[2]^l[1]&l[2]);l=[w+x|0].concat(l),l[4]=l[4]+w|0}for(d=0;8>d;d++)l[d]=l[d]+r[d]|0}for(d=0;8>d;d++)for(e=3;e+1;e--){var y=l[d]>>8*e&255;i+=(16>y?0:"")+y.toString(16)}return i};

/*Author of the code below : Lallemand Allan 
Done for Master Thesis : Atomic asset transfer between Multichain and Hyperledger Fabric.

This transaction filter for Multichain enforces the conditions of a HTLC formated as seen in "transaction_filter_send_htlc.js".
It will first look in the input to detect HTLC and then, verifies in the outputs if at least one satisfies the conditions.
To unlock the hashlock, it will require to put the correct preimage in a field named "preimage".
Until that the first parameter is misspelled or missing in the json, this transction filter will verify that the conditions to get the assets are respected by the party and will reject the transaction if not.

In simpler terms, the code of this program can be resumed as :
- If all HTLCs present in the inputs of the transaction have their conditions respected in at least one output, accepts the transaction
- Otherwise, rejects the transaction
- Exiges a field "preimage" to unlock the hashlock 

Since that the transaction filter must be given to the Multichain JSON-RPC API, the code must be written on only one line.
Before use, it is necessary to remove all end-of-line (whoch is also the reason why there is no comment with "//") 
*/


/*Verifies if a preimage is present in the transaction and if yes, verifies that the hash of the preimage matches the hash given in the conditions of the HTLC using the right hashing functions.
If these two conditions are fulfilled,return true.
Otherwise, return false.
*/
function hashlockManagement(jsonOutput,jsonInput)
{	
	if ("preimage" in jsonOutput)
	{
		if (jsonInput.hashlockHashFunction == "sha256" && sha256(jsonOutput.preimage) == jsonInput.hashlockHash) /*To add more hashing functions, add the  corresponding conditions here, the codes for the hashing functions above this function and the name of these hashing in the array of the file "transaction_filter_send_htlc.js"*/
		{	
			return true;
		}
		else	
		{
			return false; /*Wrong preimage*/
		}
	}
	else
	{
		return false; /*Preimage missing*/
	}
}

/*Verifies if the actual block height is superior or equal to the block height indicated in the conditions of the HTLC.
If yes,return true.
Otherwise, return false.
*/
function timelockManagement(jsonInput)
{	
	var actualBlock=getlastblockinfo().height;
	if (actualBlock>=jsonInput.blockHeightUnlock)
	{	
		return true;
	}	
	else 
	{	
		return false; /*Timelock not finished*/
	}
}

/*Verifies which condition must be tested according to the address that sends this transaction
Return true if the condition was verified
Retrun false if the condtion was not verified or if a third-party (not a participant of this HTLC) has made this transaction*/
function jsonOutputManagement(jsonOutput,senderAddress,receiverAddress,jsonInput,addressOutput)
{	if (addressOutput == senderAddress)
	{		
		return timelockManagement(jsonInput);
	}
	else if (addressOutput == receiverAddress)
	{
		return hashlockManagement(jsonOutput,jsonInput);
	}
	else
	{
		return false;	
	}
}

/*
Look in the json of the selected output to verify if the conditions of the selected HTLC are fulfilled or not in this json.
Return true if yes.
Return false if no. 
*/
function dataOutputManagement(data,senderAddress,receiverAddress,jsonInput,addressOutput)
{	
	if ("json" in data)
	{	
		return jsonOutputManagement(data.json,senderAddress,receiverAddress,jsonInput,addressOutput);
	}
	else
	{	
		return false;	
	}
}


/*
For all data fields in this output, verifies in the condition of the selected HTLC are respected or not.
Return True if yes. Otherwise, returns False.
Returning False will not lead to the immediate rejection of the transaction since that the other output of this transactions could fulfill these requirements.
*/
function voutManagement(vout,senderAddress,receiverAddress,jsonInput)
{	
	if ("data" in vout)
	{
		var n = 0;
		var addressOutput = vout.scriptPubKey.addresses[0];
		var conditionRespected = false;
		while (n < vout.data.length && conditionRespected == false) /*Iterates until all data fields of this output were inspected or that one of the condtions of the inspected HTLC was fulfilled*/
		{			
			conditionRespected = dataOutputManagement(vout.data[n],senderAddress,receiverAddress,jsonInput,addressOutput);
			n = n + 1;
		}
		return conditionRespected;
	}
	else
	{	
		return false;
	}
}

/*
Look in a data field of the input if there are a HTLC and if yes, begins the verification.
Return null if there are no HTLC here or if all HTLC present in this data field have seen their conditions respected.
Otherwise, if a HTLC does not have these conditions respected, returns an error.
*/
function jsonInputManagement(json,vouts)
{	
	if ("mode" in json && json.mode=="htlc")
	{
		/*HTLC detected in the input*/
		var jsonInput = json;
		var senderAddress = json.senderAddress;
		var receiverAddress = json.receiverAddress;
		var n = 0;
		var conditionRespected = false;
		while (n < vouts.length && conditionRespected == false)
		{
			conditionRespected = voutManagement(vouts[n],senderAddress,receiverAddress,jsonInput); /*Inspection of all output*/
			n = n + 1;
		}
		if (conditionRespected == false)
		{
			return "At least one htlc input does not have its conditions fulfullied in the outputs"; /*Will lead to the immediate rejection of the transaction*/
		}
		else
		{
			return null;
		}
	}
	else
	{
		return null;
	}
}

/*
Look in the json of the selected data field of the selected input to detect HTLC and verify if the conditions of all HTLC present on this input are fulfilled or not.
Return true if yes.
Return false if no. 
*/
function dataInputManagement(data,vouts)
{	
	if ("json" in data)	
	{
		return jsonInputManagement(data.json,vouts);
	}
	else
	{
		return null;	
	}
}

/*Verify in the selected input if all HTLCs have there condition fulfilled in the outputs of the transaction
If all HTLC (if any) in the inputs have their conditions respected, return null (and the transaction will be accepted)
Otherwise, if at least a HTLC does not have its conditions respected in at least one of the outputs of the transactions, return an error (and the transaction will be rejected)*/
function inputManagement(input,vouts)
{	var resultLastData = null;
	if ("data" in input)
	{
		var n = 0;
		while (n < input.data.length && resultLastData == null)
		{
			resultLastData = dataInputManagement(input.data[n],vouts); /*Inspection of all data fields*/
			n = n + 1;
		}
		return resultLastData;
	}
	else
	{
		return null;
	}
}

/*Main function 
 Look if there a HTLC in the inputs of the transactions and verifies if the conditions of the detected HTLC are fulfilled are not.
 If at least one HTLC does not sees these conditions respected, the transaction is rejected. */
function filtertransaction()
{	
	var vouts=getfiltertransaction().vout; /*Contains outputs of the transactions*/
	var inputs=getfiltertransaction().vin; 
	var n = 0;
	var resultLastData = null; /*Variable to detect and shows errors*/
	while (n < inputs.length && resultLastData == null)
	{
		resultLastData = inputManagement(getfiltertxinput(n),vouts); /*Iterates with getfiltertxinput will allow to verify all inputs of the transaction*/
		n = n + 1;
	}
	if (resultLastData != null)
	{
		return resultLastData;	
	}	
}
