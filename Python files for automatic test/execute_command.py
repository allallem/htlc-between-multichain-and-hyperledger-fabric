from constants import *
import re
import os
import subprocess
import pty
import time

def execute_command(command,wait=3):
	if type(command) == str:
		command = command.split(" ")
	output_process_command = subprocess.run(command,capture_output=True) # remove firstly '\n' before to format for subprocess.run()
	time.sleep(wait)
	output_command_split = output_process_command.stdout.decode("utf-8").split("\n")
	stderr_command = output_process_command.stderr.decode("utf-8")
	if type(command) == str:
		#print("Command : " + command)
		pass
	if type(command) == list:
		#print(" ".join(command))
		pass
	print("Result : " + output_process_command.stdout.decode("utf-8"))
	print("Error : " + stderr_command)
	return output_command_split
	
def execute_daemon(command,wait=3):
	#print("Command : " + str(command.split(" ")))
	daemon_master, daemon_slave = pty.openpty() #  Code with pty comes from stackoverflow : Python Run a daemon sub-process & read stdout (Winston Evert)
	daemon_result = subprocess.Popen(command,shell=True,stdin=subprocess.PIPE,stdout=daemon_slave,stderr=daemon_slave)
	time.sleep(wait)
	daemon_result_stdout = os.fdopen(daemon_master)
	return daemon_result_stdout
	
def find_wanted_line_daemon(result_of_command,word):
	wanted_line = None
	while wanted_line == None:
		actual_line = result_of_command.readline()
		#print(actual_line)
		if word in actual_line:
			wanted_line = actual_line
	return wanted_line[:-1]
	
def find_wanted_line_command(result_of_command,regex):
	line = ""
	number_line = 0
	print("New")
	while not re.search(regex,line):
		line = result_of_command[number_line]
		number_line += 1
		print(line,number_line)
	return line
	
def find_wanted_number_line_command(result_of_command,regex):
	line = ""
	number_line = 0
	while not re.search(regex,line):
		line = result_of_command[number_line]
		number_line += 1
	return number_line
	
def find_wanted_line_command_after_line(result_of_command,regex,line_number):
	line = ""
	number_line = line_number
	while not re.search(regex,line):
		line = result_of_command[number_line]
		number_line += 1
	return line
	
	
def find_wanted_number_line_command_after_line(result_of_command,regex,line_number):
	line = ""
	number_line = line_number
	while not re.search(regex,line):
		line = result_of_command[number_line]
		number_line += 1
	return number_line
	
def find_wanted_sequence_in_line(line,regex):
	return re.findall(regex,line)[0]
	
def find_wanted_sequence_command(result_of_command,regex):
	return find_wanted_sequence_in_line(find_wanted_line_command(result_of_command,regex),regex)
	
def find_wanted_sequence_daemon(result_of_command,regex):
	return find_wanted_sequence_in_line(find_wanted_line_daemon(result_of_command,regex),regex)
