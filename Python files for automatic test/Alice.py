from sockets_functions import *
from constants import *
from execute_command import *
from common_functions import *
from AutoPython import *
import secrets
import hashlib
import time
import openpyxl
import sys

timer_before_all_Alice = None
timer_after_all_Alice = None

timer_before_multisigaddress = None
timer_after_multisigaddress = None

timer_Alice_before_send_asset = None
timer_Alice_after_send_asset = None


timer_before_transaction_filter = None
timer_after_transaction_filter = None


timer_Alice_before_receive_asset = None
timer_Alice_after_receive_asset = None


def random_data_generator():
	sec = secrets.token_hex(256)
	print("----------------------")
	print("Preimage : " + sec)
	print("----------------------")
	return sec

def connect_to_admin():
	socket_trustworthy_admin = create_client_socket(IP_ADDRESS,PORT_SOCKET_TRUSTWORTHY_ADMIN)
	send_message(socket_trustworthy_admin,NUMBER_ALICE)
	receive_message(socket_trustworthy_admin)
	return socket_trustworthy_admin
	
def connect_malicious_admin_and_bob_to_Alice():
	socket_malicious_admin,socket_bob = create_server_sockets(PORT_SOCKET_ALICE,2)
	send_message(socket_malicious_admin,"Hello from Alice to malicious admin")
	send_message(socket_bob,"Hello from Alice to Bob")
	return (socket_malicious_admin,socket_bob)
	
def wait_for_transaction_filter_HTLC(socket_trustworthy_admin):
	receive_message(socket_trustworthy_admin)
	
def ask_token_to_admin(socket_trustworthy_admin,alice_address):
	send_message(socket_trustworthy_admin,alice_address)
	receive_message(socket_trustworthy_admin)
	

def create_multisigaddress_HTLC_Alice(socket_bob,alice_public_key):
	bob_public_key = receive_message(socket_bob)
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_ALICE + " " + "addmultisigaddress" + " " + "1" + " " + write_two_public_keys(alice_public_key,bob_public_key),0)
	multisigaddress_HTLC = result[0]
	#print(multisigaddress_HTLC)
	send_message(socket_bob,alice_public_key)
	#print("Multisigaddress created Alice-side")
	receive_message(socket_bob)
	return multisigaddress_HTLC
	
def ask_permission_for_permissions_of_multisigaddress_HTLC(socket_malicious_admin,socket_trustworthy_admin,multisigaddress):
	ask_permission_for_permissions_of_multisigaddress_HTLC_send(socket_trustworthy_admin,multisigaddress)
	if CONSENSUS_NEEDED:
		ask_permission_for_permissions_of_multisigaddress_HTLC_send(socket_malicious_admin,multisigaddress)
	
def ask_permission_for_permissions_of_multisigaddress_HTLC_send(socket_admin,multisigaddress):
	send_message(socket_admin,"Ask permission for multisigaddress" + STRING_SEPARATOR + multisigaddress)
	receive_message(socket_admin)

def send_asset_on_HTLC_multisigaddress(socket_malicious_admin,socket_bob,multisigaddress,alice_address):
	global timer_before_transaction_filter
	global timer_after_transaction_filter
	send_message(socket_bob,"Alice Ready")
	bob_address = receive_message(socket_bob)
	#print("Bob : " + bob_address)
	block_number,time_number = get_block_number_and_time(RECURRENT_OPTIONS_ALICE)
	hashlock_preimage = random_data_generator()
	hashlock_hash = hashlib.sha256(hashlock_preimage.encode("utf-8")).hexdigest()
	incomplete_command = 'send <MultiSignatureAddress> {"<asset>":<amount>,"data":{"json":{"mode":"htlc","senderAddress":"<AddressAlice>","receiverAddress":"<AddressBob>","blockHeightUnlock":<endBlock>,"hashlockHash":"<hash_value>","hashlockHashFunction":"<hash_function>"}}}'
	incomplete_command = incomplete_command.replace("<MultiSignatureAddress>",multisigaddress)
	incomplete_command = incomplete_command.replace("<asset>",ASSET_NAME)
	incomplete_command = incomplete_command.replace("<amount>","1000")
	incomplete_command = incomplete_command.replace("<AddressAlice>",alice_address)
	incomplete_command = incomplete_command.replace("<AddressBob>",bob_address)
	incomplete_command = incomplete_command.replace("<endBlock>","10") # TODO: Change value
	incomplete_command = incomplete_command.replace("<hash_value>",hashlock_hash)
	complete_command = incomplete_command.replace("<hash_function>","sha256")
	timer_before_transaction_filter = time.perf_counter() # Timer (verification of transaction filter)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_ALICE + " " + complete_command,0)
	timer_after_transaction_filter = time.perf_counter() # Timer (verification of transaction filter)
	send_message(socket_bob,"Asset send on Multisigaddress")
	return hashlock_preimage,bob_address
	
def multichain_before_HLF_Alice():
	global timer_before_all_Alice
	global timer_before_multisigaddress
	global timer_after_multisigaddress
	global timer_Alice_before_send_asset
	global timer_Alice_after_send_asset
	global timer_Alice_before_receive_asset
	socket_malicious_admin,socket_bob = connect_malicious_admin_and_bob_to_Alice()
	socket_trustworthy_admin = connect_to_admin()
	connect_to_blockchain(socket_trustworthy_admin,RECURRENT_OPTIONS_ALICE,ALICE_DIRECTORY)
	alice_address,alice_public_key = get_address_and_public_key(RECURRENT_OPTIONS_ALICE) # Outside timer since that could be stored in local
	wait_for_transaction_filter_HTLC(socket_trustworthy_admin)
	ask_token_to_admin(socket_trustworthy_admin,alice_address)
	show_tokens(RECURRENT_OPTIONS_ALICE)
	send_message(socket_bob,"Begin Timer") # Timer starts
	timer_before_all_Alice = time.perf_counter() # Timer (all after initialisation of the blockchain, admins permissions and issuing asset to create the test)
	timer_before_multisigaddress = timer_before_all_Alice # Timer (creation and permission of the multisigaddress)
	multisigaddress = create_multisigaddress_HTLC_Alice(socket_bob,alice_public_key)
	ask_permission_for_permissions_of_multisigaddress_HTLC(socket_malicious_admin,socket_trustworthy_admin,multisigaddress)
	timer_after_multisigaddress = time.perf_counter() # Timer (creation and permission of the multisigaddress)
	timer_Alice_before_send_asset = timer_after_multisigaddress # Timer (send asset on the multisigaddress on Multichain)
	hashlock_preimage,bob_address = send_asset_on_HTLC_multisigaddress(socket_malicious_admin,socket_bob,multisigaddress,alice_address)
	timer_Alice_after_send_asset = time.perf_counter() # Timer (send asset on the multisigaddress on Multichain)
	timer_Alice_before_receive_asset = timer_Alice_after_send_asset # Timer (receive asset on Hyperledger Fabric)
	#show_tokens(RECURRENT_OPTIONS_ALICE)
	return (hashlock_preimage,socket_bob,socket_malicious_admin,bob_address,multisigaddress,alice_address)
	
"""
The code comes from start.sh of "https://github.com/Daniel-Szego/AtomicCrossChain/blob/master/HLF/start.sh".
Original author : Daniel Szego
Original title : Atomic cross chain swap between Hyperledger Fabric and Ethereum
Original presentation : "https://www.youtube.com/watch?v=j_j2MiAxUvY"
The code has simply been translated in Python and removed some steps from the original code
"""
def HLF_after_multichain_Bob(socket_bob,hashlock_preimage,socket_malicious_admin,bob_address):
	#execute_command(HLF_QUERY + " " + '{"Args":["GetBalance","Alice"]}',0)
	send_message(socket_malicious_admin,bob_address) # Ask for attack
	receive_message(socket_bob)
	execute_command(HLF_QUERY + " " + '{"Args":["GetHashTimeLock","htlc1"]}')
	#execute_command(HLF_QUERY + " " + '{"Args":["GetBalance","Alice"]}')
	incomplete_sentence_with_preimage = '{"Args":["Commit","htlc1","<password>","50"]}'
	complete_sentence_with_preimage = incomplete_sentence_with_preimage.replace("<password>",hashlock_preimage)
	execute_command(HLF_INVOKE + " " + complete_sentence_with_preimage)


	send_message(socket_bob,"Preimage sent")
	#execute_command(HLF_QUERY + " " + '{"Args":["GetBalance","Alice"]}',0)
	
def recover_asset_on_multichain(multisigaddress,alice_address):
	wait_time = 100
	#print("Waiting " + str(wait_time) + " seconds for timelock expiration")
	time.sleep(wait_time)
	unlock_metadata(RECURRENT_OPTIONS_ALICE)
	incomplete_command = 'createrawsendfrom <MultiSignatureAddress> {"<AddressAlice>":{"<asset>":1000,"data":{"json":{"timelock":"true"}}}} [] send'
	incomplete_command = incomplete_command.replace("<MultiSignatureAddress>",multisigaddress)
	incomplete_command = incomplete_command.replace("<AddressAlice>",alice_address)
	complete_command = incomplete_command.replace("<asset>",ASSET_NAME)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_ALICE + " " + complete_command)
	
def calculate_timer():
	global timer_before_multisigaddress
	global timer_after_multisigaddress
	global timer_Alice_before_send_asset
	global timer_Alice_after_send_asset
	global timer_before_transaction_filter
	global timer_after_transaction_filter
	global timer_Alice_before_receive_asset
	global timer_Alice_after_receive_asset
	global timer_before_all_Alice
	global timer_after_all_Alice
	time_multisigaddress = timer_after_multisigaddress - timer_before_multisigaddress
	time_Alice_send_asset = timer_Alice_after_send_asset - timer_Alice_before_send_asset
	time_transaction_filter = timer_after_transaction_filter - timer_before_transaction_filter
	time_Alice_receive_asset = timer_Alice_after_receive_asset - timer_Alice_before_receive_asset
	time_all = timer_after_all_Alice - timer_before_all_Alice
	print(f"multisigaddress : {time_multisigaddress:0.9f} seconds")
	print(f"send : {time_Alice_send_asset:0.9f} seconds")
	print(f"transaction : {time_transaction_filter:0.9f} seconds")
	print(f"receive : {time_Alice_receive_asset:0.9f} seconds")
	print(f"all : {time_all:0.9f} seconds")
	write_in_file_xslx_Alice("Alice.xlsx",time_multisigaddress,time_Alice_send_asset,time_transaction_filter,time_Alice_receive_asset,time_all)
	
def write_in_file_xslx_Alice(file_xslx,time_multisigaddress,time_Alice_send_asset,time_transaction_filter,time_Alice_receive_asset,time_all): # From https://stackoverflow.com/questions/34767174/how-to-write-data-into-existing-xlsx-file-which-has-multiple-sheets Martin Evans Jan 13, 2016
	new_row = [time_all,time_multisigaddress, time_Alice_send_asset, time_transaction_filter, time_Alice_receive_asset,sys.argv[1]]

	wb = openpyxl.load_workbook(filename=file_xslx)
	ws = wb['Sheet1']
	row = ws.max_row + 1

	for col, entry in enumerate(new_row, start=1):
    		ws.cell(row=row, column=col, value=entry)
	wb.save(file_xslx)
	
hashlock_preimage,socket_bob,socket_malicious_admin,bob_address,multisigaddress,alice_address = multichain_before_HLF_Alice()
HLF_after_multichain_Bob(socket_bob,hashlock_preimage,socket_malicious_admin,bob_address)
timer_Alice_after_receive_asset = time.perf_counter() # Timer (receive asset on Hyperledger Fabric)
timer_after_all_Alice = timer_Alice_after_receive_asset
show_tokens(RECURRENT_OPTIONS_ALICE)
#recover_asset_on_multichain(multisigaddress,alice_address)
#show_tokens(RECURRENT_OPTIONS_ALICE)
calculate_timer()
