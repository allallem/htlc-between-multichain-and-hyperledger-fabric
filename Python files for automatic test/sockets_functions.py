import socket
from constants import *
import time

def create_server_sockets(port,number_clients):
	server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#print(port)
	server_socket.bind(("0.0.0.0",port))
	#server_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)
	server_socket.listen(number_clients)
	list_clients_with_role = accept_all_clients(number_clients,server_socket)
	return sort_by_role(list_clients_with_role)
	
def accept_all_clients(number_clients,server_socket):
	list_clients_with_role = []
	while len(list_clients_with_role) < number_clients:
		new_person_socket = server_socket.accept()[0]
		#print("Client seen")
		role = new_person_socket.recv(1024)
		list_clients_with_role.append((new_person_socket,role))
	return list_clients_with_role
	
def sort_by_role(list_clients_with_role):
	sorted_list_clients_with_role = sorted(list_clients_with_role,key=lambda l: l[1])
	sorted_list_clients = []
	for client in sorted_list_clients_with_role:
		sorted_list_clients.append(client[0])
	return sorted_list_clients
		
def create_client_socket(host,port):
	socket_communication = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#socket_communication.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)
	socket_communication.connect((host,port))
	return socket_communication

def receive_message(socket_communication):
	message = (socket_communication.recv(1024)).decode("utf-8")
	#print("Receive : " + message)
	return message
	
def receive_two_strings(socket_communication):
	message = (socket_communication.recv(1024)).decode("utf-8")
	message_1,message_2 = message.split(STRING_SEPARATOR)
	#print("Receive : " + message_1)
	#print("Receive : " + message_2)
	return (message_1,message_2)

def send_message(socket_communication,message):
	#print("Send : " + message)
	message = message.encode("utf-8")
	socket_communication.send(message)
