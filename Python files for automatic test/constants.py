import os
import random

ID = 10

os.environ['CORE_PEER_TLS_ENABLED'] = "true"
os.environ['CORE_PEER_LOCALMSPID'] = "Org1MSP"
os.environ['CORE_PEER_TLS_ROOTCERT_FILE'] = "./organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt"
os.environ['CORE_PEER_MSPCONFIGPATH'] = "./organizations/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp"
os.environ['CORE_PEER_ADDRESS'] = "localhost:7051"

NUMBER_MALICIOUS_ADMIN = "0"
NUMBER_ALICE = "1"
NUMBER_BOB = "2"

IP_ADDRESS = "127.0.0.6"

D = 0

PORT_SOCKET_TRUSTWORTHY_ADMIN = 2000 + D
PORT_SOCKET_ALICE = 2001 + D

CHAIN_NAME = "chain"

MULTICHAIND = "../../multichainMod/src/multichaind"
MULTICHAIN_CLI = "../../multichainMod/src/multichain-cli"
MULTICHAIN_UTIL = "../../multichainMod/src/multichain-util"

PORT_MULTICHAIN_TRUSTWORTHY_ADMIN = 3000 + D
PORT_MULTICHAIN_RPC_TRUSTWORTHY_ADMIN = 3001 + D
PORT_MULTICHAIN_MALICIOUS_ADMIN = 3002 + D
PORT_MULTICHAIN_RPC_MALICIOUS_ADMIN = 3003 + D
PORT_MULTICHAIN_ALICE = 3004 + D
PORT_MULTICHAIN_RPC_ALICE = 3005 + D
PORT_MULTICHAIN_BOB = 3006 + D
PORT_MULTICHAIN_RPC_BOB = 3007 + D

CHAIN_CONNECTION = CHAIN_NAME + "@" + IP_ADDRESS + ":" + str(PORT_MULTICHAIN_TRUSTWORTHY_ADMIN)

ALICE_DIRECTORY = "../../multichainUserDirectory/Alice"
BOB_DIRECTORY = "../../multichainUserDirectory/Bob"
MALICIOUS_ADMIN_DIRECTORY = "../../multichainUserDirectory/Malicious_admin"
TRUSTWORTHY_ADMIN_DIRECTORY = "../../multichainUserDirectory/Trustworthy_admin"


OPTIONS_CREATION_BLOCKCHAIN = "-admin-consensus-send=1 -admin-consensus-receive=1 -admin-consensus-connect=1 -admin-consensus-txfilter=1 -admin-consensus-admin=1 -setup-first-blocks=1"
RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN = "-port=" +  str(PORT_MULTICHAIN_TRUSTWORTHY_ADMIN) + " -rpcport=" + str(PORT_MULTICHAIN_RPC_TRUSTWORTHY_ADMIN) + " -datadir=" + TRUSTWORTHY_ADMIN_DIRECTORY
RECURRENT_OPTIONS_MALICIOUS_ADMIN = "-port=" +  str(PORT_MULTICHAIN_MALICIOUS_ADMIN) + " -rpcport=" + str(PORT_MULTICHAIN_RPC_MALICIOUS_ADMIN) + " -datadir=" + MALICIOUS_ADMIN_DIRECTORY
RECURRENT_OPTIONS_ALICE = "-port=" +  str(PORT_MULTICHAIN_ALICE) + " -rpcport=" + str(PORT_MULTICHAIN_RPC_ALICE) + " -datadir=" + ALICE_DIRECTORY
RECURRENT_OPTIONS_BOB = "-port=" +  str(PORT_MULTICHAIN_BOB) + " -rpcport=" + str(PORT_MULTICHAIN_RPC_BOB) + " -datadir=" + BOB_DIRECTORY

ASSET_NAME = "AssetHTLC"

CONSENSUS_NEEDED = 1

NAME_TRANSACTION_FILTER_SEND_ASSET = "HTLC_SEND_ASSET"
NAME_TRANSACTION_FILTER_RECEIVE_ASSET = "HTLC_RECEIVE_ASSET"

FILE_TRANSACTION_FILTER_SEND_ASSET = "transaction_filter_send_htlc.js"
FILE_TRANSACTION_FILTER_RECEIVE_ASSET = "transaction_filter_receive_htlc.js"

FILE_NAME_LASTBLOCK = "lastblock.block"

HLF_INVOKE = "peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls --cafile ./organizations/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n basic --peerAddresses localhost:7051 --tlsRootCertFiles ./organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ./organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c"

HLF_QUERY = "peer chaincode query -C mychannel -n basic -c"

HLF_FETCH = "peer channel fetch newest " + FILE_NAME_LASTBLOCK + " -c mychannel"

STRING_SEPARATOR = "|"
