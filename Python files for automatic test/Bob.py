from sockets_functions import *
from constants import *
from execute_command import *
from common_functions import *
from AutoPython import *
import datetime
import sys
import openpyxl

timer_before_all = None # Blockchain creation ignored in the measure of time
timer_after_all = None

timer_Bob_before_multisig = None
timer_Bob_after_multisig = None

timer_Bob_before_hash_and_verfication = None
timer_Bob_after_hash_and_verfication = None

timer_Bob_before_get_transaction = None
timer_Bob_after_get_transaction = None

timer_Bob_before_verification_conditions = None
timer_Bob_after_verification_conditions = None

timer_Bob_before_search_hash = None
timer_Bob_after_search_hash = None


timer_Bob_before_send_asset = None
timer_Bob_after_send_asset = None

timer_Bob_before_search_preimage = None
timer_Bob_after_search_preimage = None

timer_before_transaction_filter2 = None
timer_after_transaction_filter2 = None

timer_Bob_before_receive_asset = None
timer_Bob_after_receive_asset = None


def create_multisigaddress_HTLC_Bob(socket_alice,bob_public_key):
	send_message(socket_alice,bob_public_key)
	alice_public_key = receive_message(socket_alice)
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_BOB + " " + "addmultisigaddress" + " " + "1" + " " + write_two_public_keys(alice_public_key,bob_public_key),0)
	multisigaddress_HTLC = result[0]
	#print(multisigaddress_HTLC)
	send_message(socket_alice,"Multisigaddress created Bob-side")
	return multisigaddress_HTLC
	
def allow_alice_send(socket_alice,bob_address):
	receive_message(socket_alice)
	send_message(socket_alice,bob_address)
	
def learn_hashlock_hash(socket_alice,multisigaddress,bob_address):
	global timer_Bob_before_search_hash
	global timer_Bob_after_search_hash
	global timer_Bob_before_verification_conditions
	global timer_Bob_after_verification_conditions
	global timer_Bob_before_get_transaction
	global timer_Bob_after_get_transaction
	receive_message(socket_alice)
	get_block_number_and_time(RECURRENT_OPTIONS_BOB) # Verify conditions
	timer_Bob_before_get_transaction = time.perf_counter()
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_BOB + " " + "listaddresstransactions" + " " + multisigaddress + " " + "1" + " " + "0" + " " + "true")
	timer_Bob_after_get_transaction = time.perf_counter()
	timer_Bob_before_verification_conditions = time.perf_counter() # Timer ()
	verification_metadata(result,multisigaddress,bob_address)
	calculation_time(result)
	timer_Bob_after_verification_conditions = time.perf_counter() # Timer ()
	timer_Bob_before_search_hash = time.perf_counter() # Timer ()
	line_conditions = find_wanted_line_command(result,"hashlockHash")
	hashlock_hash = find_wanted_sequence_in_line(line_conditions,"[0-9a-f]{64}")
	timer_Bob_after_search_hash = time.perf_counter() # Timer ()
	print(hashlock_hash)
	return hashlock_hash
	
	
def calculation_time(result):
	actual_block_number,actual_time = get_block_number_and_time(RECURRENT_OPTIONS_BOB)
	blockchain_params = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_BOB + " " + "getblockchainparams")
	target_block_time = find_wanted_sequence_in_line(blockchain_params[6],"[0-9]+")
	print("==========")
	print(target_block_time)
	print("==========")
	try:
		end_block = find_wanted_sequence_in_line(result[65],"[0-9]+")
	except:
		end_block = find_wanted_sequence_in_line(result[69],"[0-9]+")
	print("Traget : " + str(target_block_time))
	difference_block = int(end_block) - int(actual_block_number)
	seconds_timelock = difference_block * int(target_block_time)
	print("End : " + str(end_block))
	print(seconds_timelock)
	return seconds_timelock
	
	
def verification_metadata(result,multisigaddress,bob_address):
	error_transaction = True
	print("OK")
	if (result[45].find(multisigaddress)!=-1): # Verification that the assets are on the Multisigaddress
		print("Pass1")
		if (result[53].find(ASSET_NAME)!=-1): # Verification of the type of assets
			print("Pass2")
			if result[55].find("1000")!=-1: # Verification of the number of assets
				print("Pass3")
				if (result[61].find("json")!=-1):
					print("Pass4")
					if result[62].find('"mode" : "htlc"')!=-1:
						print("Pass5")
						if result[64].find(bob_address)!=-1:
							error_transaction = False
							print("Pass6")
	if (error_transaction == True):
		print("Second try")
		time.sleep(3)
		if (result[49].find(multisigaddress)!=-1): # Verification that the assets are on the Multisigaddress
			print("Pass1")
			if (result[57].find(ASSET_NAME)!=-1): # Verification of the type of assets
				print("Pass2")
				if (result[59].find("1000")!=-1): # Verification of the number of assets
					print("Pass3")
					if (result[65].find("json")!=-1):
						print("Pass4")
						if result[66].find('"mode" : "htlc"')!=-1:
							print("Pass5")
							if result[68].find(bob_address)!=-1:
								error_transaction = False
								print("Pass6")
	if (error_transaction == True):
		print("Incorrect amount. End of program")
		print(result[45])
		print(result[49])
		sys.exit(1)
	
	
	
def multichain_before_HLF_Bob():
	global timer_before_all
	global timer_Bob_before_multisig
	global timer_Bob_after_multisig
	global timer_Bob_before_send_asset
	global timer_Bob_before_hash_and_verfication
	global timer_Bob_after_hash_and_verfication
	socket_trustworthy_admin,socket_alice = connect_to_other(NUMBER_BOB)
	connect_to_blockchain(socket_trustworthy_admin,RECURRENT_OPTIONS_BOB,BOB_DIRECTORY)
	bob_address,bob_public_key = get_address_and_public_key(RECURRENT_OPTIONS_BOB) # Outside timer since that could be stored in local
	receive_message(socket_alice) # Begin timer
	timer_before_all = time.perf_counter() # Timer (all after initialisation of the blockchain, admins permissions and issuing asset to create the test)
	timer_Bob_before_multisig = timer_before_all # Timer (creation and permission of Multisigaddress + send of Alice)
	multisigaddress = create_multisigaddress_HTLC_Bob(socket_alice,bob_public_key)
	allow_alice_send(socket_alice,bob_address)
	timer_Bob_after_multisig = time.perf_counter() # Timer (creation and permission of Multisigaddress + send of Alice)
	timer_Bob_before_hash_and_verfication = timer_Bob_after_multisig
	hashlock_hash = learn_hashlock_hash(socket_alice,multisigaddress,bob_address)
	timer_Bob_after_hash_and_verfication = time.perf_counter()
	timer_Bob_before_send_asset = time.perf_counter() # Timer (send assets on Hyperledger Fabric)
	return (hashlock_hash,multisigaddress,bob_address,socket_alice)

"""
The code comes from start.sh of "https://github.com/Daniel-Szego/AtomicCrossChain/blob/master/HLF/start.sh".
Original author : Daniel Szego
Original title : Atomic cross chain swap between Hyperledger Fabric and Ethereum
Original presentation : "https://www.youtube.com/watch?v=j_j2MiAxUvY"
The code has simply been translated and Python and removed some steps from the original code + add how to get last block
"""
def HLF_after_multichain_Bob(socket_alice,hashlock_hash):
	global timer_Bob_after_send_asset
	global timer_Bob_before_search_preimage
	global timer_Bob_after_search_preimage
	global timer_Bob_before_receive_asset
	time_in_3_minutes = (datetime.datetime.now(datetime.timezone.utc)+datetime.timedelta(minutes=3)).isoformat()
	print(time_in_3_minutes)
	#execute_command(HLF_QUERY + " " + '{"Args":["GetBalance","Bob"]}',0)
	execute_command(HLF_INVOKE + " " + '{"Args":["TransferConditional","htlc1","Bob","Alice","50","'+ hashlock_hash +'","'+ str(time_in_3_minutes) + '"]}')
	send_message(socket_alice,"Asset Transfered")
	timer_Bob_after_send_asset = time.perf_counter() # Timer (send assets on Hyperledger Fabric)
	timer_Bob_before_search_preimage = timer_Bob_after_send_asset # Timer (search of preimage, wait for Alice)
	#execute_command(HLF_QUERY + " " + '{"Args":["GetBalance","Bob"]}')
	receive_message(socket_alice)
	execute_command(HLF_FETCH,3) # Look in the block could be more secure than looking in the smart contract (risk of replacement of the value)
	hashlock_preimage = get_preimage_in_block()
	timer_Bob_after_search_preimage = time.perf_counter() # Timer (search of preimage, wait for Alice)
	timer_Bob_before_receive_asset = timer_Bob_after_search_preimage # Timer (receive asset on Multichain)
	return hashlock_preimage

def get_preimage_in_block():
	with open("lastblock.block","rb") as lastblock_HLF:
		found = False
		while not found:
			lastblock_HLF_line = lastblock_HLF.readline()
			preimage_sentence = b"success commit with preimage : "
			if preimage_sentence in lastblock_HLF_line:
				hashlock_preimage = lastblock_HLF_line.partition(preimage_sentence)[2]
				hashlock_preimage = hashlock_preimage.partition(STRING_SEPARATOR.encode("utf-8"))[0]
				found = True
#	#print(hashlock_preimage)
	return hashlock_preimage.decode("utf-8")
	
def get_asset_on_HTLC_multisigaddress(multisigaddress,bob_address,hashlock_preimage):
	global timer_before_transaction_filter2
	global timer_after_transaction_filter2
	global timer_Bob_after_receive_asset
	unlock_metadata(RECURRENT_OPTIONS_BOB)

	incomplete_command = 'createrawsendfrom <MultiSignatureAddress> {"<AddressBob>":{"<asset>":1000,"data":{"json":{"preimage":"<preimage_value>"}}}} [] send'
	incomplete_command = incomplete_command.replace("<MultiSignatureAddress>",multisigaddress)
	incomplete_command = incomplete_command.replace("<AddressBob>",bob_address)
	incomplete_command = incomplete_command.replace("<asset>",ASSET_NAME)
	complete_command = incomplete_command.replace("<preimage_value>",hashlock_preimage)
	timer_before_transaction_filter2 = time.perf_counter() # Timer (Transaction filter)
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_BOB + " " + complete_command,0)
	timer_after_transaction_filter2 = time.perf_counter() # Timer (Transaction filter)
	timer_Bob_after_receive_asset = timer_after_transaction_filter2 # Timer (receive asset on Multichain)


	
def multichain_after_HLF_Bob(hashlock_preimage,multisigaddress,bob_address):
	global timer_after_all
	#show_tokens(RECURRENT_OPTIONS_BOB)
	get_asset_on_HTLC_multisigaddress(multisigaddress,bob_address,hashlock_preimage)
	timer_after_all = time.perf_counter() # Timer (all after initialisation of the blockchain, admins permissions and issuing asset to create the test)
	show_tokens(RECURRENT_OPTIONS_BOB)

	
def calculate_timer():
	global timer_before_all
	global timer_after_all
	global timer_Bob_before_search_hash
	global timer_Bob_after_search_hash
	global timer_Bob_before_send_asset
	global timer_Bob_after_send_asset
	global timer_Bob_before_search_preimage
	global timer_Bob_after_search_preimage
	global timer_before_transaction_filter2
	global timer_Bob_before_receive_asset
	global timer_Bob_after_receive_asset
	global timer_Bob_before_multisig
	global timer_Bob_after_multisig
	global timer_Bob_before_verification_conditions
	global timer_Bob_after_verification_conditions
	global timer_Bob_before_hash_and_verfication
	global timer_Bob_after_hash_and_verfication
	global timer_Bob_before_get_transaction
	global timer_Bob_after_get_transaction
	time_all = timer_after_all - timer_before_all
	time_Bob_multisig = timer_Bob_after_multisig - timer_Bob_before_multisig
	time_Bob_search_hash = timer_Bob_after_search_hash - timer_Bob_before_search_hash
	time_Bob_send_asset = timer_Bob_after_send_asset - timer_Bob_before_send_asset
	time_Bob_search_preimage = timer_Bob_after_search_preimage - timer_Bob_before_search_preimage
	time_transaction_filter2 = timer_after_transaction_filter2 - timer_before_transaction_filter2
	time_Bob_receive_asset = timer_Bob_after_receive_asset - timer_Bob_before_receive_asset
	time_Bob_verification_conditions = timer_Bob_after_verification_conditions - timer_Bob_before_verification_conditions
	time_Bob_hash_verification_conditions = timer_Bob_after_hash_and_verfication - timer_Bob_before_hash_and_verfication
	time_Bob_get_transaction = timer_Bob_after_get_transaction - timer_Bob_before_get_transaction
	print(f"all : {time_all:0.9f} seconds")
	print(f"multisig : {time_Bob_multisig:0.9f} seconds")
	print(f"hash_verifications : {time_Bob_hash_verification_conditions:0.9f} seconds")
	print(f"get_transactions : {time_Bob_get_transaction:0.9} seconds")
	print(f"verifications : {time_Bob_verification_conditions:0.9f} seconds")
	print(f"hash : {time_Bob_search_hash:0.9f} seconds")
	print(f"send : {time_Bob_send_asset:0.9f} seconds")
	print(f"preimage : {time_Bob_search_preimage:0.9f} seconds")
	print(f"receive : {time_Bob_receive_asset:0.9f} seconds")
	print(f"transaction : {time_transaction_filter2:0.9f} seconds")
	write_in_file_xslx_Bob("Bob.xlsx",time_all,time_Bob_multisig,time_Bob_hash_verification_conditions,time_Bob_get_transaction,time_Bob_verification_conditions,time_Bob_search_hash,time_Bob_send_asset,time_Bob_search_preimage,time_Bob_receive_asset,time_transaction_filter2)

def write_in_file_xslx_Bob(file_xslx,time_all,time_Bob_multisig,time_Bob_hash_verification_conditions,time_Bob_get_transaction,time_Bob_verification_conditions,time_Bob_search_hash,time_Bob_send_asset,time_Bob_search_preimage,time_Bob_receive_asset,time_transaction_filter2): # From https://stackoverflow.com/questions/34767174/how-to-write-data-into-existing-xlsx-file-which-has-multiple-sheets Martin Evans Jan 13, 2016
	new_row = [time_all,time_Bob_multisig,time_Bob_hash_verification_conditions,time_Bob_get_transaction,time_Bob_verification_conditions,time_Bob_search_hash,time_Bob_send_asset,time_Bob_search_preimage,time_Bob_receive_asset,time_transaction_filter2,sys.argv[1]]

	wb = openpyxl.load_workbook(filename=file_xslx)
	ws = wb['Sheet1']     # Older method was  .get_sheet_by_name('Sheet1')
	row = ws.max_row + 1

	for col, entry in enumerate(new_row, start=1):
    		ws.cell(row=row, column=col, value=entry)
	wb.save(file_xslx)
	
execute_command(HLF_INVOKE + " " + '{"Args":["MintToken","Bob","200"]}')	
hashlock_hash,multisigaddress,bob_address,socket_alice = multichain_before_HLF_Bob()
hashlock_preimage = HLF_after_multichain_Bob(socket_alice,hashlock_hash)
multichain_after_HLF_Bob(hashlock_preimage,multisigaddress,bob_address)
calculate_timer()
