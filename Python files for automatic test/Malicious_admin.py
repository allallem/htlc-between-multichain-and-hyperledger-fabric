from sockets_functions import *
from constants import *
from execute_command import *
from common_functions import *
	
def ask_admin_priviledge(socket_trustworthy_admin,recurrent_options):
	address = get_address_and_public_key(recurrent_options)[0]
	send_message(socket_trustworthy_admin,address)
	
def approve_transaction_filter_malicious_admin(socket_trustworthy_admin,malicious_admin_address):
	if CONSENSUS_NEEDED:
		receive_message(socket_trustworthy_admin)
		approve_transaction_filter(malicious_admin_address,RECURRENT_OPTIONS_MALICIOUS_ADMIN)
		send_message(socket_trustworthy_admin,"Transaction filter approved")
	
def grant_permissions_to_multisigaddress_HTLC_and_send_message_malicious_admin(socket_alice):
	multisigaddress = None
	if CONSENSUS_NEEDED:
		multisigaddress = grant_permissions_to_multisigaddress_HTLC(socket_alice,RECURRENT_OPTIONS_MALICIOUS_ADMIN)
		send_message(socket_alice,"Permissions granted to multisigaddress (with consensus)")
		
def multichain_before_HLF_Malicous_admin():
	socket_trustworthy_admin,socket_alice = connect_to_other(NUMBER_MALICIOUS_ADMIN)
	connect_to_blockchain(socket_trustworthy_admin,RECURRENT_OPTIONS_MALICIOUS_ADMIN,MALICIOUS_ADMIN_DIRECTORY)
	malicious_admin_address,malicious_admin_public_key = get_address_and_public_key(RECURRENT_OPTIONS_MALICIOUS_ADMIN)
	ask_admin_priviledge(socket_trustworthy_admin,RECURRENT_OPTIONS_MALICIOUS_ADMIN)
	approve_transaction_filter_malicious_admin(socket_trustworthy_admin,malicious_admin_address)
	grant_permissions_to_multisigaddress_HTLC_and_send_message_malicious_admin(socket_alice)
	return socket_alice

def attack(socket_alice):
	bob_address = receive_message(socket_alice)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_MALICIOUS_ADMIN + " " + "revoke" + " " + bob_address + " " + "receive")

socket_alice = multichain_before_HLF_Malicous_admin()
attack(socket_alice)
