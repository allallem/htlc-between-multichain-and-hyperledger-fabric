Put all .py and .js files in the test-network directory of hyperledger fabric.

Change the constant CONSENSUS_NEEDED from 1 to 0 to see the attack suceeds because only one admin is required to make change of permissions.


Run network.sh down
Run network.sh up createChannel
Run deployChaincode.sh
Run Trustworthy_admin.py
Wait that "Ready" appears on the terminal that runs Trustworthy_admin.py
Run Alice.py
Run Bob.py and Malicious_admin.py
Use InitalState.sh to kill the process on the port used
