from execute_command import *
from sockets_functions import *
import time

def connect_to_blockchain(socket_trustworthy_admin,recurrent_options,directory):
	execute_command("mkdir -p " + directory)
	result = execute_daemon(MULTICHAIND + " " + CHAIN_CONNECTION + " " + recurrent_options,5)
	command_to_connect = find_wanted_line_daemon(result,"connect,send,receive")	# Get the command for permissions to connect,receive,send
	send_message(socket_trustworthy_admin,command_to_connect)
	receive_message(socket_trustworthy_admin)
	result = execute_daemon(MULTICHAIND + " " + CHAIN_NAME  + " " + recurrent_options + " -daemon",5)
	find_wanted_line_daemon(result,"Node ready.")
	time.sleep(10)
	
def get_address_and_public_key(recurrent_options):
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_CONNECTION  + " " + recurrent_options + " getaddresses true",10)
	address_line = find_wanted_line_command(result,"address")
	address = find_wanted_sequence_in_line(address_line,"[0-9A-Za-z]{8,}")
	pubkey_line = find_wanted_line_command(result,"pubkey")
	public_key = find_wanted_sequence_in_line(pubkey_line,"[0-9a-z]{7,}")
	return (address,public_key)
	
def connect_to_other(number_role):
	socket_alice = create_client_socket(IP_ADDRESS,PORT_SOCKET_ALICE)
	send_message(socket_alice,number_role)
	socket_trustworthy_admin = create_client_socket(IP_ADDRESS,PORT_SOCKET_TRUSTWORTHY_ADMIN)
	send_message(socket_trustworthy_admin,number_role)
	receive_message(socket_trustworthy_admin)
	receive_message(socket_alice)
	return (socket_trustworthy_admin,socket_alice)
	

def write_two_public_keys(public_key_alice,public_key_bob):
	string = '["<PublicKeyAlice>","<PublicKeyBob>"]'
	string1 = string.replace("<PublicKeyAlice>",public_key_alice)
	string2 = string1.replace("<PublicKeyBob>",public_key_bob)
	return string2 
	
def approve_transaction_filter(address_admin,recurrent_options):
	for tf in [NAME_TRANSACTION_FILTER_SEND_ASSET,NAME_TRANSACTION_FILTER_RECEIVE_ASSET]:
		incomplete_command = "approvefrom <AddressAdmin>" + " " + tf + " " + "true"
		command = incomplete_command.replace("<AddressAdmin>",address_admin)
		execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + command)
	
def grant_permissions_to_multisigaddress_HTLC(socket_alice,recurrent_options):
	useless_string,multisigaddress = receive_two_strings(socket_alice)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + "grant " + multisigaddress + " send,receive")
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + "listpermissions" + " send,receive")
	return multisigaddress

def get_block_number_and_time(recurrent_options):
	result = execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + "getlastblockinfo")
	block_line = find_wanted_line_command(result,"height")
	block_number = find_wanted_sequence_in_line(block_line,"[0-9]+")
	time_line = find_wanted_line_command(result,"time")
	time_number = find_wanted_sequence_in_line(time_line,"[0-9]+")
	return (block_number,time_number)

def show_tokens(recurrent_options):
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + "gettotalbalances" + " " + "0")

def unlock_metadata(recurrent_options):
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + recurrent_options + " " + "setruntimeparam" + " " + "lockinlinemetadata" + " " + "false")
	
