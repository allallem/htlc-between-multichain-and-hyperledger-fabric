from sockets_functions import *
from constants import *
from execute_command import *
from common_functions import *
import time



def connect_everyone_to_trustworthy_admin():
	socket_malicious_admin,socket_alice,socket_bob = create_server_sockets(PORT_SOCKET_TRUSTWORTHY_ADMIN,3)
	send_message(socket_malicious_admin,"Hello from Trustworthy admin to malicious admin")
	send_message(socket_alice,"Hello from Trustworthy admin to Alice")
	send_message(socket_bob,"Hello from Trustworthy admin to Bob")
	return (socket_malicious_admin,socket_alice,socket_bob)
	
def create_blockchain():
	execute_command("mkdir -p " + TRUSTWORTHY_ADMIN_DIRECTORY)
	if CONSENSUS_NEEDED:
		execute_command(MULTICHAIN_UTIL + " create " + CHAIN_NAME + " -datadir=" + TRUSTWORTHY_ADMIN_DIRECTORY + " " + OPTIONS_CREATION_BLOCKCHAIN,0)
	else:
		execute_command(MULTICHAIN_UTIL + " create " + CHAIN_NAME + " -datadir=" + TRUSTWORTHY_ADMIN_DIRECTORY,0)
	result = execute_daemon(MULTICHAIND + " " + CHAIN_NAME + " -daemon " + RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN,0)
	find_wanted_line_daemon(result,"Node ready.")
	
def connect_everyone_to_blockchain(socket_malicious_admin,socket_alice,socket_bob):
	list_sockets = [socket_malicious_admin,socket_alice,socket_bob] 
	for sock in list_sockets:
		command_for_connection = receive_message(sock)
		execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN + " " + remove_multichain_cli_and_chain_name(command_for_connection),0)
		send_message(sock,"Connection authorised")
		
def remove_multichain_cli_and_chain_name(command_for_connection):
	command_for_connection = command_for_connection.split(" ")
	command_for_connection = command_for_connection[2:]
	striped_command_for_connection = ""
	for elem in command_for_connection:
		striped_command_for_connection = striped_command_for_connection + elem + " "
	return striped_command_for_connection[:-1]
	
def grant_admin_priviledge_to_malicious_admin(socket_malicious_admin):
	malicious_admin_address = receive_message(socket_malicious_admin)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN + " " + "grant " + malicious_admin_address + " admin",0)
	
def create_HTLC_transaction_filter():
	list_transcation_filter_and_name = [(FILE_TRANSACTION_FILTER_SEND_ASSET,NAME_TRANSACTION_FILTER_SEND_ASSET),(FILE_TRANSACTION_FILTER_RECEIVE_ASSET,NAME_TRANSACTION_FILTER_RECEIVE_ASSET)]
	for transaction_filter_and_name in list_transcation_filter_and_name:
		with open(transaction_filter_and_name[0],"r") as tf_file:
			incomplete_command = MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN + " " + "create" + " " "txfilter" + " " + transaction_filter_and_name[1] + " " + '{}' + " "  + '<content_of_transaction_filter>'
			incomplete_splited_command = incomplete_command.split(" ")
			transaction_filter_content = tf_file.read().replace('\n',"")
			incomplete_splited_command[-1] = transaction_filter_content
			splited_command = incomplete_splited_command
			execute_command(splited_command,0)
		
def approve_transaction_filter_trustworthy_admin(socket_malicious_admin,socket_alice,trustworthy_admin_address):
	approve_transaction_filter(trustworthy_admin_address,RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN)
	if CONSENSUS_NEEDED:
		send_message(socket_malicious_admin,"Request for approval for transaction filter")
		receive_message(socket_malicious_admin)
	send_message(socket_alice,"Transaction filter approved")
		
	
def issue_token_on_Alice_address(socket_alice):
	alice_address = receive_message(socket_alice)
	execute_command(MULTICHAIN_CLI + " " + CHAIN_NAME + " " + RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN + " " + "issue" + " " + alice_address + " "  + ASSET_NAME + " " + "1000" + " " + "0.01")
	send_message(socket_alice,"Asset issued")
	
def grant_permissions_to_multisigaddress_HTLC_and_send_message_trustworthy_admin(socket_alice):
	grant_permissions_to_multisigaddress_HTLC(socket_alice,RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN)
	if CONSENSUS_NEEDED:
		send_message(socket_alice,"Wait consensus for grant permissions")
	else:
		send_message(socket_alice,"Permissions granted to multisigaddress")	
		
def multichain_before_HLF_Trustworthy_admin():
	create_blockchain()
	print("Ready !")	
	socket_malicious_admin,socket_alice,socket_bob = connect_everyone_to_trustworthy_admin()
	connect_everyone_to_blockchain(socket_malicious_admin,socket_alice,socket_bob)
	trustworthy_admin_address,trustworthy_admin_public_key = get_address_and_public_key(RECURRENT_OPTIONS_TRUSTWORTHY_ADMIN)
	grant_admin_priviledge_to_malicious_admin(socket_malicious_admin)
	create_HTLC_transaction_filter()
	approve_transaction_filter_trustworthy_admin(socket_malicious_admin,socket_alice,trustworthy_admin_address)
	issue_token_on_Alice_address(socket_alice)
	grant_permissions_to_multisigaddress_HTLC_and_send_message_trustworthy_admin(socket_alice)
		
multichain_before_HLF_Trustworthy_admin()

